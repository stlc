(ns lambda.tli_tests
  (:use [lambda.tli])
  (:refer-clojure :exclude [==])
  (:use [clojure.core.logic :exclude [is] :as l])
  (:use [clojure.test]))

(deftest test-tio-forward
  (is (= (run* [t]
               (tio '([x 1]) 'x t))
         '(1)))
  (is (= (run* [t]
               (tio '() '(fn [x] x)  t))
         '([:=> _0 _0])))
  (is (= (run* [t]
               (tio '() '((fn [x] x) (fn [x] x))  t))
         '([:=> _0 _0])))
  (is (= (run* [t]
               (tio '() '(fn [f] (fn [x] (f x))) t))
         '([:=> [:=> _0 _1] [:=> _0 _1]]))))

(deftest test-tio-backwards
  (is (= (run 3 [e]
              (tio '() e [:=> 'a 'a]))
         '(((fn [_0] _0) :- (sym _0))
           ((fn [_0] ((fn [_1] _1) _0)) :- (sym _0) (sym _1))
           (((fn [_0] _0) (fn [_1] _1)) :- (sym _0) (sym _1)))))
  (is (= (run 3 [e]
              (fresh [a]
                     (tio '() e [:=> a a])))
         '(((fn [_0] _0) :- (sym _0))
           ((fn [_0] (fn [_1] _1)) :- (sym _1))
           (((fn [_0] _0) (fn [_1] _1)) :- (sym _0) (sym _1))))))

(deftest test-tio-generate
  (is (= (run 10 [q]
              (fresh [e t]
                     (== q [e t])
                     (tio '() e t)))
         '(([(fn [_0] _0) [:=> _1 _1]] :- (sym _0))
           ([(fn [_0] (fn [_1] _1)) [:=> _2 [:=> _3 _3]]] :- (sym _1))
           ([(fn [_0] (fn [_1] _0)) [:=> _2 [:=> _3 _2]]] :- (sym _0) (!= (_0 _1)))
           ([((fn [_0] _0) (fn [_1] _1)) [:=> _2 _2]] :- (sym _0) (sym _1))
           ([(fn [_0] (fn [_1] (fn [_2] _2))) [:=> _3 [:=> _4 [:=> _5 _5]]]] :- (sym _2))
           ([(fn [_0] (fn [_1] (fn [_2] _1))) [:=> _3 [:=> _4 [:=> _5 _4]]]] :- (sym _1) (!= (_1 _2)))
           ([(fn [_0] (_0 (fn [_1] _1))) [:=> [:=> [:=> _2 _2] _3] _3]] :- (sym _0) (sym _1))
           ([((fn [_0] _0) (fn [_1] (fn [_2] _2))) [:=> _3 [:=> _4 _4]]] :- (sym _0) (sym _2))
           ([(fn [_0] (fn [_1] (fn [_2] _0))) [:=> _3 [:=> _4 [:=> _5 _3]]]] :- (sym _0) (!= (_0 _2)) (!= (_0 _1)))
           ([(fn [_0] ((fn [_1] _1) _0)) [:=> _2 _2]] :- (sym _0) (sym _1))))))

(deftest test-env-lookupo
  (is (= (run* [q]
               (env-lookupo '([x 1]) 'x q))
         '(1)))
  (is (= (run* [q]
               (env-lookupo '() 'x q))
         '()))
  (is (= (run* [q]
               (fresh [a b]
                      (env-lookupo `([~a 1] [~b 2]) 'x q)))
         '(1 2)))
  (is (= (run* [q]
               (fresh [a b]
                      (env-lookupo `([~a 1] [~'x 2] [~b 3]) 'x q)))
         '(1 2))))
