(ns lambda.tli
  (:refer-clojure :exclude [==])
  (:use [clojure.core.logic :exclude [is] :as l]))

;;; utilities
(defn reifier-for [tag x]
  (fn [c v r a]
    (let [x (walk* r (walk* a x))]
      (when (symbol? x)
        `(~tag ~x)))))
(defn symbolo [x] (predc x symbol? (reifier-for 'sym x)))


;;; type := var | [:=> type type]
;;; exp  := name | (fn [name] exp) | (exp exp)

;;; env := () | ([name type] . env)
(defn env-lookupo [env x tx]
  (fresh [y ty env-rest]
         (conso [y ty] env-rest env)
         (conde
          ((== x y) (== tx ty))
          ((!= x y) (env-lookupo env-rest x tx)))))

(defn tio [env e t]
  (conde
   ((symbolo e) (env-lookupo env e t))
   ((fresh [x body tx tbody envx]
           (== e `(~'fn [~x] ~body))
           (conso [x tx] env envx)
           (== t [:=> tx tbody])
           (tio envx body tbody)))
   ((fresh [e1 e2 t1 t11 t12 t2]
           (== e `(~e1 ~e2))
           (== t1 [:=> t11 t12])
           (== t2 t11)
           (== t t12)
           (tio env e1 t1)
           (tio env e2 t2)))))
