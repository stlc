(defproject tapl "1.0.0-SNAPSHOT"
  :description "TAPL in core.logic"
  :url "https://github.com/namin/TAPL-in-miniKanren-cKanren-core.logic"
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/core.logic "0.8.3"]]
)
